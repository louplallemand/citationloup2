import { Request, Response } from "express-serve-static-core";

export default class QuoteController
{
    static index(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        let allquote : any = []
        let allauthor: any = []
        let quote = db.prepare('SELECT * FROM quote LIMIT 10 OFFSET ?').all(((Number(req.params.page)-1)*10))
        let author = db.prepare('SELECT * FROM author').all()

        author.forEach((e:any) => {
            allauthor.push(e.id)
        })
        
        if(quote.length == 1){
            allquote.push({
                content: quote[0].content,
                author: author[allauthor.indexOf(quote[0].author_id)].name
            })
        }else{
            quote.forEach((e:any) => {
                allquote.push({
                    content: e.content,
                    author: author[allauthor.indexOf(e.author_id)].name
                })
            });
        }

        let total = db.prepare('SELECT COUNT(*) FROM quote').all()
        let pages: number = Math.ceil((total[0]['COUNT(*)'])/10)
        let i: number = 0
        let pagenum: any = [] 
        while (i<pages){
            i++
            pagenum.push(i)
        }

        res.render('pages/quote', {
            quote: allquote,
            page: pagenum
        });
    }
}