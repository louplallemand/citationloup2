import { Request, Response } from "express-serve-static-core";
import { idText } from "typescript";

export default class PanelController
{
    static index(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        let allquote : any = []
        let allauthor: any = []
        let quote = db.prepare('SELECT * FROM quote').all()
        let author = db.prepare('SELECT * FROM author').all()
        
        author.forEach((e:any) => {
            allauthor.push(e.id)
        })
        quote.forEach((e:any) => {
            allquote.push({
                content: e.content,
                author: author[allauthor.indexOf(e.author_id)].name,
                id: e.id
            })
        });
        

        let authors = db.prepare('SELECT * FROM author').all()

        res.render('pages/panel', {
            quote: allquote,
            author: authors
        });
    }

    static createquote(req: Request, res: Response): void 
    {
        const db = req.app.locals.db;
        let allauthor: any = []
        let quote = db.prepare('SELECT * FROM quote').all()
        let author = db.prepare('SELECT * FROM author').all()

        author.forEach((e:any) => {
            allauthor.push(e.name)
        })

        if (req.body.quote !== '' && req.body.authorquote !== ''){
            if (allauthor.includes(req.body.authorquote)){
                db.prepare('INSERT INTO quote ("content","author_id") VALUES (?,?)').run(req.body.quote, author[allauthor.indexOf(req.body.authorquote)].id)
            }else{
                db.prepare('INSERT INTO author ("name") VALUES (?)').run(req.body.authorquote)
                let theauthor = db.prepare('SELECT * FROM author WHERE name = ?').get(req.body.authorquote)
                db.prepare('INSERT INTO quote ("content","author_id") VALUES (?,?)').run(req.body.quote, theauthor.id)
            }
        }
        
        res.redirect('/panel');
    }

    static deletequote(req: Request, res: Response): void 
    {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM quote WHERE id = ?').run(req.params.id)

        res.redirect('/panel');
    }

    static updatequote(req: Request, res: Response): void 
    {
        const db = req.app.locals.db;
        let allauthor: any = []
        let author = db.prepare('SELECT * FROM author').all()

        author.forEach((e:any) => {
            allauthor.push(e.name)
        })
        
        if (req.body.authorquote !== ''){
            if (allauthor.includes(req.body.authorquote)){
                db.prepare('UPDATE quote SET content = ?, author_id = ? WHERE id = ?').run(req.body.quote,author[allauthor.indexOf(req.body.authorquote)].id,req.body.id)
            }else{
                db.prepare('INSERT INTO author ("name") VALUES (?)').run(req.body.authorquote)
                let theauthor = db.prepare('SELECT * FROM author WHERE name = ?').get(req.body.authorquote)
                db.prepare('UPDATE quote SET content = ?,author_id = ? WHERE id = ?').run(req.body.quote, theauthor.id,req.body.id)
            }
        }
        
        res.redirect('/panel');
    }

    static createauthor(req: Request, res: Response): void 
    {
        const db = req.app.locals.db;
        let author = db.prepare('SELECT * FROM author').all()
        let allauthor: any = []

        author.forEach((e: any) => {
            allauthor.push(e.name)
        });

        if (!allauthor.includes(req.body.author) && req.body.author !== ''){
            db.prepare('INSERT INTO author ("name") VALUES (?)').run(req.body.author)
        }
        res.redirect ('/panel')
    }

    static deleteauthor(req: Request, res: Response): void 
    {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM quote WHERE author_id = ?').run(req.params.id)
        db.prepare('DELETE FROM author WHERE id = ?').run(req.params.id)

        res.redirect ('/panel')
    }

    static updateauthor(req: Request, res: Response): void 
    {
        const db = req.app.locals.db;

        db.prepare('UPDATE author SET name = ? WHERE id = ?').run(req.body.author, req.body.id)

        res.redirect ('/panel')
    }
}