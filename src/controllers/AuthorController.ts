import e from "express";
import { Request, Response } from "express-serve-static-core";
import { idText } from "typescript";

export default class AuthorController
{
    static index(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        let authorWithQuote : any = []
        let allauthor: any = []
        let author = db.prepare('SELECT * FROM author').all()
        let check = db.prepare('SELECT * FROM quote').all()

        author.forEach((e:any) => {
            allauthor.push(e.id)
        })
        check.forEach((e:any) => {
            if (!authorWithQuote.includes(author[allauthor.indexOf(e.author_id)])){
                authorWithQuote.push(author[allauthor.indexOf(e.author_id)])
            }
        })
        
        res.render('pages/author', {
            author: authorWithQuote
        });
    }

    static quotes(req: Request, res: Response): void{
        const db = req.app.locals.db;

        let name = req.params
        let authorid = db.prepare('SELECT id FROM author WHERE name = ?').get(name.author)

        if(authorid !==undefined){
            let quotes = db.prepare('SELECT * FROM quote WHERE author_id = ?').all(authorid.id)            
            
            res.render('pages/authorquote', {
                quote: quotes,
                name: name.author
            });
        }else{
            let error = [{
                content: "Il n'y a pas d'auteur portant ce nom ici"
            }]
            res.render('pages/authorquote', {
                quote: error
            });
        }
    }
}