import { Request, Response } from "express-serve-static-core";
import { idText } from "typescript";

export default class HomeController
{
    static index(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        let quote = db.prepare('SELECT * FROM quote').all()
        let thequote = quote[quote.length-1]

        if (thequote !== undefined){
            let author = db.prepare('SELECT * FROM author WHERE id = ?').get(thequote.author_id)

            res.render('pages/index', {
                quote: thequote,
                author: author,
            });
        }else{
            res.render('pages/index', {
                quote: {content:'pas de citation pour le moment'},
                author: {name:''},
            });
        }
    }
}