import { Application } from "express";
import HomeController from "./controllers/HomeController";
import QuoteController from "./controllers/QuoteController";
import AuthorController from "./controllers/AuthorController";
import PanelController from "./controllers/PanelController";
import { ParsedQs } from "qs";
//import { redirectLog } from "./server";
import { Request, ParamsDictionary, Response } from "express-serve-static-core";
import { body, validationResult } from 'express-validator';


export default function route(app: Application) {
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.index(req, res);
    });

    app.get('/quote/:page', (req, res) =>
    {
        QuoteController.index(req, res);
    });

    app.get('/author', (req, res) =>
    {
        AuthorController.index(req, res);
    })

    app.get('/panel', (req, res) =>
    {
        PanelController.index(req, res);
    })

    app.post('/createquote', (req, res) =>
    {
        PanelController.createquote(req, res);
    })

    app.get('/deletequote/:id', (req, res) =>
    {
        PanelController.deletequote(req, res);
    })

    app.post('/updatequote', (req, res) =>
    {
        PanelController.updatequote(req, res);
    })

    app.post('/createauthor', (req, res) =>
    {
        PanelController.createauthor(req, res);
    })

    app.get('/deleteauthor/:id', (req, res) =>
    {
        PanelController.deleteauthor(req, res);
    })

    app.post('/updateauthor', (req, res) =>
    {
        PanelController.updateauthor(req, res);
    })

    app.get('/:author', (req, res) =>
    {
        req.params.author
        AuthorController.quotes(req, res);
    })
}




